#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

int main(void)
{
	double* p=(double*)1000;
	long A, B, C;

#define p

	/* YOUR CODE HERE */
	/* note: sizeof(double) == 8 */

#undef p

	assert(A==(long)p);
	assert(B==(long)&p[3]);

	p+=2;

	assert(C==(long)p);

	printf("correct!\n");

	return 0;
}
