#include <stdio.h>
#include <assert.h>
#include <string.h>

/* YOUR CODE HERE */

int main(void)
{
	int x;
	float y;
	double z;

	x = DOUBLE(1);
	y = DOUBLE(2);
	z = DOUBLE(3);

	assert(x==2);
	assert(y==4);
	assert(z==6);

	printf("correct!\n");

	return 0;
}
