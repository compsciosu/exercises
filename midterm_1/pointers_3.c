#include <stdio.h>
#include <assert.h>
#include <string.h>

/* YOUR CODE HERE */

int main(void)
{
	int x=1, y=2;

	swap(&x, &y);

	assert(x==2);
	assert(y==1);

	printf("correct!\n");

	return 0;
}
