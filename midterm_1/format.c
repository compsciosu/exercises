#include <stdio.h>
#include <assert.h>
#include <string.h>

int main(void)
{
	int i=1;
	float j=1.23;
	char s[128];

	char* format_string="/* YOUR CODE HERE */";

	sprintf(s, format_string, i, j);

	assert(strcmp(s, "001, 1.2")==0);

	printf("correct!\n");
	
	return 0;
}
