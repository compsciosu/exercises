#include <stdio.h>
#include <assert.h>
#include <string.h>

int main(void)
{
	int x, y, z;
	int a, b, c;

	x=0, y=0, z=++x&&++y;

#define x
#define y
#define z

	/* YOUR CODE HERE */

#undef x
#undef y
#undef z

	assert(a==x); assert(b==y); assert(c==z);

	x=0, y=0, z=++x&&y++;

#define x
#define y
#define z

	/* YOUR CODE HERE */

#undef x
#undef y
#undef z

	assert(a==x); assert(b==y); assert(c==z);

	x=0, y=0, z=x++&&++y;

#define x
#define y
#define z

	/* YOUR CODE HERE */

#undef x
#undef y
#undef z

	assert(a==x); assert(b==y); assert(c==z);

	x=0, y=0, z=x++&&y++;

#define x
#define y
#define z

	/* YOUR CODE HERE */

#undef x
#undef y
#undef z

	assert(a==x); assert(b==y); assert(c==z);

	x=0, y=0, z=++x||++y;

#define x
#define y
#define z

	/* YOUR CODE HERE */

#undef x
#undef y
#undef z

	assert(a==x); assert(b==y); assert(c==z);

	x=0, y=0, z=++x||y++;

#define x
#define y
#define z

	/* YOUR CODE HERE */

#undef x
#undef y
#undef z

	assert(a==x); assert(b==y); assert(c==z);

	x=0, y=0, z=x++||++y;

#define x
#define y
#define z

	/* YOUR CODE HERE */

#undef x
#undef y
#undef z

	assert(a==x); assert(b==y); assert(c==z);

	x=0, y=0, z=x++||y++;

#define x
#define y
#define z

	/* YOUR CODE HERE */

#undef x
#undef y
#undef z

	assert(a==x); assert(b==y); assert(c==z);

	printf("correct!\n");

	return 0;
}
