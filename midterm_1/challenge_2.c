#include <stdio.h>
#include <assert.h>
#include <string.h>

#define ELEMS 100000

int main(void)
{
	int i;
	int a[ELEMS];

#define do
#define while
#define for

	/* YOUR CODE HERE */
	/* Note - the "challenge" problems are more complex than what will be on the
	 * midterm, and are designed to stretch your understanding. */

#undef do
#undef while
#undef for

	for (i=0; i<ELEMS; i++) {
		assert(a[i]==i);
	}

	printf("correct!\n");

	return 0;
}
