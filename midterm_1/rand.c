#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

int rand_1_to_3(void)
{
	/* YOUR CODE HERE */
}

int main(void)
{
	int i, j;
	int found[]={0,0,0};

	for (i=0; i<100; i++) {
		j=rand_1_to_3();
		assert(j==1 || j==2 || j==3);
		found[j-1]=1;
	}

	assert(found[0]&&found[1]&&found[2]);

	printf("correct!\n");

	return 0;
}
