#include <stdio.h>
#include <assert.h>
#include <string.h>

int main(void)
{
	int A, B, C, D, E, F, G, H, I;
	int x;
	int *y;
	char **z;

	#define sizeof(...)

	/* YOUR CODE HERE */
	/* COMPILE WITH THE -m32 FLAG! */

	#undef sizeof

	assert(A==sizeof(x));
	assert(B==sizeof(&x));

	assert(C==sizeof(y));
	assert(D==sizeof(&y));
	assert(E==sizeof(*y));

	assert(F==sizeof(z));
	assert(G==sizeof(&z));
	assert(H==sizeof(*z));
	assert(I==sizeof(**z));

	printf("correct!\n");

	return 0;
}
