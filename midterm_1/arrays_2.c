#include <stdio.h>
#include <assert.h>
#include <string.h>

/* YOUR CODE HERE */

int main(void)
{
	int a[2][3];

	zero(a, 2, 3);

	assert(a[0][0]==0);
	assert(a[0][1]==0);
	assert(a[0][2]==0);
	assert(a[1][0]==0);
	assert(a[1][1]==0);
	assert(a[1][2]==0);

	printf("correct!\n");

	return 0;
}
