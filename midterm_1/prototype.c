#include <stdio.h>
#include <assert.h>
#include <string.h>

/* YOUR CODE HERE */
/* fix the compiler warning when compiling with -Wall */

int main(void)
{
	int x=add(1, 2);

	assert(x==3);

	printf("correct!\n");

	return 0;
}

int add(int x, int y)
{
	return x+y;
}
