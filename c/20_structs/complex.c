#include <stdio.h>

typedef struct {
	float a, b;
} complex_t;

complex_t add_complex(complex_t, complex_t);
void print_complex(complex_t);

int main(void)
{
	complex_t x={1, 2}; /* 1 + 2i */
	complex_t y={-3, 0}; /* -3 + 0i */

	complex_t z=add_complex(x, y);

	print_complex(z);

	return 0;
}

complex_t add_complex(complex_t x, complex_t y)
{
	/* YOUR CODE HERE */
	/* add the complex numbers x and y together, and return the result */
}

void print_complex(complex_t x)
{
	/* YOUR CODE HERE */
	/* print out the fields of the complex number x using printf */
}
