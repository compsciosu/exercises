#include <ncurses.h>

/* create a structure to hold a screen coordinate */
typedef struct { int row, col; } screen_coord_t;

/* create a structure to hold a cartesian coordinate */
typedef struct { float x, y; } cartesian_coord_t;

/* the dimensions of the cartesian system */
const float cartesian_min_x=-1;
const float cartesian_max_x=1;
const float cartesian_min_y=-1;
const float cartesian_max_y=1;

/* convert from screen coordinates to cartesian coordinates */
cartesian_coord_t screen_to_cartesian(screen_coord_t s, int screen_width, int screen_height)
{
	cartesian_coord_t c;

	/* YOUR CODE HERE */
	/* convert the screen coordinate s to the corresponding cartesian coordinate c */
	
	/* For example: the screen coordinate 0, 0 represents the upper left corner
	 * of the screen - this should become the upper left coordinate in the
	 * cartesian system - in our case, (-1, 1).  If the screen is 80 characters
	 * wide and 40 characters high, the screen coordinate (20, 40) is the center
	 * - it would become the center point on the cartesian system - (0, 0).
	 */

	/* Use the scaling formula: 
	 *   f(x) = C+(D-C)*(x-A)/(B-A)
	 * to convert between ranges.  The range [A,B] is the screen dimensions, the
	 * range [C,D] is the cartesian dimensions, and x is a value to transform.
	 * */

	/* Don't forget about the consequences of integer division! */

	/* Remember to link with -lncurses */

	return c;
}

/* returns true if the cartesian point is in the set, false otherwise */
int is_in_set(cartesian_coord_t c)
{
	float r=c.x*c.x+c.y*c.y;

	if (r>1) {
		return 0;
	}
	else if ((c.x+.3f)*(c.x+.3f)+(c.y+.3f)*(c.y+.3f)<.025f) {
		return 0;
	}
	else if ((c.x-.3f)*(c.x-.3f)+(c.y+.3f)*(c.y+.3f)<.025f) {
		return 0;
	}
	else if (r>.3f && r<.4f && c.y>0) {
		return 0;
	}

	return 1;
}

/* a demonstration of coordinate conversions */
int main(void)
{
	int max_row, max_col;
	int x, y;

	/* ncurses */
	/* with ncurses, we _must_ call initscr() before calling any other ncurses
	 * functions */
	initscr();

	/* ncurses */
	/* the ncurses getmaxyx function gives us the dimensions of the console */
	getmaxyx(stdscr, max_row, max_col);

	/* iterate over each character on the screen */
	for (y=0; y<max_row; y++) {
		for (x=0; x<max_col; x++) {
			/* ncurses */
			/* move the cursor to the current position */
			move(y, x);

			/* calculate the current position in screen coordinates */
			screen_coord_t s={y, x};

			/* convert the screen coordinate to a cartesian coordinate */
			cartesian_coord_t c=screen_to_cartesian(s, max_col, max_row);

			/* check if the point is in the set */
			if (is_in_set(c)) {
				/* ncurses */
				/* print an x character at this location */
				addch('x');
			}
			else {
				/* ncurses */
				/* print space at this location */
				addch(' ');
			}
		}
	}

	/* ncurses */
	/* after adding character to the screen, it must be refreshed before we see
	 * anything */
	refresh();

	/* ncurses */
	/* read a character in from the user to pause before we exit */
	getchar();

	/* ncurses */
	/* when we are done with ncurses, we call endwin to stop using ncurses */
	endwin();

	return 0;
}
