#include <stdio.h>
#include <stdlib.h>

float dart(void);

int main(void)
{
	int hits=0, throws=0;

	while (1) {
		float pi, x, y;

		/* throw a dart at the board */
		x=dart();
		y=dart();
		throws++;

		/* check if the dart landed in the circle */
		if (x*x+y*y<=1) {
			hits++;
		}

		/* the ratio of hits to throws lets us calculate pi! */
		printf("hits: %5d, throws: %5d, pi estimate: %f\n", 
				hits,
				throws,
				(float)hits/throws*4
				);
	}

	return 0;
}

float dart(void)
{
	/* YOUR CODE HERE */
	/* rand() returns a random integer between 0 and RAND_MAX */
	/* use rand() and division to return a random float between -1 and 1 */
}
