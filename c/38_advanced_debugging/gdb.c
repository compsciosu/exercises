#define printf

/* use gdb to discover the secret message after it's been decrypted */

void decrypt(char* s)
{
	int i=0;

	while (s[i]) {
		if (s[i]>='a' && s[i]<='z') {
			s[i]='a'+(((s[i]-'a')+13)%26);
		}
		i++;
	}
}

void b(char* s)
{
	decrypt(s);
}

void a(char* s)
{
	b(s);
}

int main(void)
{
	char secret[]="nyy bgure cebtenzzvat ynathntrf ner vasrevbe gb p";
	a(secret);
	return 0;
}
