#include <stdio.h>

#if 0
int global_sum; // temporary; prof says i shouldn't use globals
#endif

int main(void)
{
	int i, sum, count;

	/* YOUR CODE HERE */
	/* Use the preprocessor directives #if and #endif to "comment out" all but
	 * the last summation */

	/* sum the numbers 0 to 100 */
	count=100;
	for (i=0; i<count; i++) {
		sum+=i;
	}
	printf("sum 0-%d: %d\n", count, sum);

	/* sum the numbers 0 to 1000 */
	count=1000;
	for (i=0; i<count; i++) {
		sum+=i;
	}
	printf("sum 0-%d: %d\n", count, sum);

	/* sum the numbers 0 to 10000 */
	count=10000;
	for (i=0; i<count; i++) {
		sum+=i;
	}
	printf("sum 0-%d: %d\n", count, sum);

	/* sum the numbers 0 to 100000 */
	count=100000;
	for (i=0; i<count; i++) {
		sum+=i;
	}
	printf("sum 0-%d: %d\n", count, sum);

	return 0;
}
