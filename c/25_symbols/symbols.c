#include <stdio.h>
#include <stdlib.h>

void debug(int line, char* file)
{
	printf("executing line %d in %s\n", line, file);
}

int main(void)
{
	float* sphere_volumes;
	int count, i;
	const float PI=3.14159;

	/* YOUR CODE HERE */
	/* 
	 * 1) A common programming approach, when you need to efficiently perform
	 * the same computations over and over, is to perform all of the
	 * calculations at once, and then store the results in a lookup table for
	 * future reference.  In this program, we compute the volumes of spheres,
	 * and then store the results for later.  This way, after the initial
	 * calculations are done, we no longer need to perform the relatively
	 * expensive computation 3/4*PI*r*r*r - we simply "look up" the value in a
	 * table.  For example, to find the volume of a sphere with radius 20, we
	 * simply look up sphere_volumes[20] - which is much faster than recomputing
	 * 3/4*PI*20*20*20.
	 *
	 * 2) Compile and run this program.  What happens?
	 *
	 * 3) Add a call to debug() at lines (A), (B), (C), and (D) to find out
	 * where the bug in the code is.  Pass the file name (__FILE__) and line
	 * number (__LINE__) to debug. 
	 *
	 */

	/* (A) */
	count=100;
	sphere_volumes=malloc(count*sizeof(float));
	for (i=0; i<count; i++) {
		/* 4/3 pi r^3 */
		sphere_volumes[i]=(float)4/3*PI*i*i*i;
	}

	/* (B) */
	i=5;
	printf("The volume of a sphere with radius %d is %f\n", i, sphere_volumes[i]);

	i=9;
	printf("The volume of a sphere with radius %d is %f\n", i, sphere_volumes[i]);

	free(sphere_volumes);

	/* (C) */
	count=3000000000;
	sphere_volumes=malloc(count*sizeof(float));
	for (i=0; i<count; i++) {
		/* 4/3 pi r^3 */
		sphere_volumes[i]=(float)4/3*PI*i*i*i;
	}

	/* (D) */
	i=519298;
	printf("The volume of a sphere with radius %d is %f\n", i, sphere_volumes[i]);

	i=91092999;
	printf("The volume of a sphere with radius %d is %f\n", i, sphere_volumes[i]);

	free(sphere_volumes);

	return 0;
}
