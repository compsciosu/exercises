#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

float speed(float distance, float time)
{
	/* YOUR CODE HERE */
	/* use assert() to validate the parameters passed to this function */
	return distance/time;
}

int main(void)
{
	printf("speed: %f\n", speed(1, 2));
	printf("speed: %f\n", speed(3, 2));
	printf("speed: %f\n", speed(8, 3));
	printf("speed: %f\n", speed(1, 7));
	printf("speed: %f\n", speed(2, 0));
	printf("speed: %f\n", speed(1, 9));
	printf("speed: %f\n", speed(7, 8));
	printf("speed: %f\n", speed(9, 1));
	return 0;
}

