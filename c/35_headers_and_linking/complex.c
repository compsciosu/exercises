#include <stdio.h>

/* YOUR CODE HERE */
/* Move the structure declarations and prototypes into the complex.h header file
 * so that they can be shared with other programs.  Modify complex.c to use the
 * new header */

typedef struct {
	float a;
	float b;
} complex_t;

complex_t complex_add(complex_t, complex_t);

int main(void)
{
	complex_t x={1, -2};
	complex_t y={-3, 1};
	complex_t z=complex_add(x, y);

	printf("z: %f%+f\n", z.a, z.b);

	return 0;
}

complex_t complex_add(complex_t x, complex_t y)
{
	return (complex_t){x.a+y.a, x.b+y.b};
}
