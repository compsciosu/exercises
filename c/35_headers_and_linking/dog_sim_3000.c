#include <stdio.h>

/* YOUR CODE HERE */
/* dog_stuff.c contains the definitions for the bark and wag functions.
 * dog_stuff.h contains the prototypes for bark and wag.  Modify this file so
 * that it can access the bark and wag functions, then compile all of the code
 * into a working executable */

int main(void)
{
	bark();
	wag();
	return 0;
}
