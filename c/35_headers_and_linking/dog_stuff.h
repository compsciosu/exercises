#ifndef DOG_STUFF_H
#define DOG_STUFF_H
/* include guards, like the directives above, help prevent compiler errors
 * caused by including a file multiple times */

void bark(void);
void wag(void);

#endif // DOG_STUFF_H
