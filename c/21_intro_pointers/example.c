#include <stdio.h>

int main(void)
{
	char string[]="i lock";
	char* p;

	printf("p is %p\n", p);

	/* (1) */
	/* compile and run this program. */
	/* what happens here? why? */

	*p=10;

	/* (2) */
	/* fix the program (remove the faulting line) */
	

	/* (3) */
	/* YOUR CODE HERE */
	/* use & to make p point to the first character of string */



	printf("p is %p\n", p);

	/* (4) */
	/* YOUR CODE HERE */
	/* using p, modify the string to say "i rock" */



	printf("p is %p\n", p);

	printf("%s\n", string);

	return 0;
}
