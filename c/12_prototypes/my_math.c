/* my_math.c */

#include "my_math.h"
#include <stdio.h>

int main(void)
{
    int a=2;
    float b=3.14;

    a=my_square(a);
    b=my_floor(b);

	printf("%d\n", a);
	printf("%f\n", b);

    return 0;
}

int my_square(int x)
{
    return x*x;
}

float my_floor(float x)
{
    return (int)x;
}
