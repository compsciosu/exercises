#include <stdio.h>

int main(void)
{
	FILE* input=fopen("input", "r");
	FILE* output=fopen("output", "w");

	if (input==NULL || output==NULL) {
		return -1;
	}

	while (!feof(input)) {
		/* YOUR CODE HERE */
		/* Use fgetc to read a character from the input file */

		/* Use rot13 to encrypt the character */
		/* (rot13 shifts the character 13 letters forward in the alphabet.
		 * Letters that move "past" 'z' wrap around back to 'a') */

		/* Use fputc to write the encrypted character to the output file */

	}

	return 0;
}
