#include <stdio.h>

int main(void)
{
	int i;
	int n=10;
	int a[n];

	/* initialize the array to 1, 2, 3, ... */
	for (i=0; i<n; i++) {
		a[i]=i;
	}

	/* print the array */
	printf("a: ");
	for (i=0; i<n; i++) {
		printf("%d ", a[i]);
	}
	printf("\n");

	/* reverse the array, _in place_ */
	/* (that is, without creating a new array */
	/* YOUR CODE HERE */

	/* print the array */
	printf("a: ");
	for (i=0; i<n; i++) {
		printf("%d ", a[i]);
	}
	printf("\n");

	return 0;
}
