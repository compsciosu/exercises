#include <stdio.h>

int main(void)
{
	int i;

	/* create 2 arrays of 8 integers */
	int x[8]={0};
	int y[8]={0};

	/* print x and y */
	printf("x: "); for (i=0; i<8; i++) { printf("%d ", x[i]); } printf("\n");
	printf("y: "); for (i=0; i<8; i++) { printf("%d ", y[i]); } printf("\n");

	/* modify x */
	/* try modifying element 0, 7, and 8 */
	printf("modifying x\n");
	x[0]=1;

	/* print x and y */
	printf("x: "); for (i=0; i<8; i++) { printf("%d ", x[i]); } printf("\n");
	printf("y: "); for (i=0; i<8; i++) { printf("%d ", y[i]); } printf("\n");

	return 0;
}
