#include <stdio.h>

#define MAX_SIZE 100

void flip(...)
{
	/* YOUR CODE HERE */
	/* flip the matrix vertically */
	/* determine what arguments flip should receive */
}

int main(void)
{
	int width, height;
	int i, j;

	/* create a matrix that stores up to MAX_SIZE x MAX_SIZE elements */
	int matrix[MAX_SIZE][MAX_SIZE];

	/* ask the user how big their matrix is */
	printf("enter width: ");
	scanf("%d", &width);
	printf("enter height: ");
	scanf("%d", &height);

	/* read in elements */
	for (i=0; i<height; i++) {
		for (j=0; j<width; j++) {
			printf("enter element %d, %d: ", i, j);
			scanf("%d", &matrix[i][j]);
		}
	}
	
	/* print the matrix */
	printf("matrix:\n");
	for (i=0; i<height; i++) {
		for (j=0; j<width; j++) {
			printf("%d ", matrix[i][j]);
		}
		printf("\n");
	}
	
	/* flip the matrix vertically */
	/* YOUR CODE HERE */
	/* (call flip() on matrix) */

	/* print the matrix */
	printf("new matrix:\n");
	for (i=0; i<height; i++) {
		for (j=0; j<width; j++) {
			printf("%d ", matrix[i][j]);
		}
		printf("\n");
	}

	return 0;
}
