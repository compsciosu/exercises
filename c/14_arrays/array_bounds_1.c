#include <stdio.h>

int main(void)
{
	/* create an array of 3 integers */
	int a[3]={1, 2, 3};

	/* print out an element of the array */
	printf("%d\n", a[0]);

	return 0;
}
