#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef struct school_t {
	char name[128];
	struct school_t* friend;
} school_t;

school_t* create_school(char* name)
{
	school_t* s=malloc(sizeof(school_t));
	assert(s);
	strcpy(s->name, name);
	s->friend=NULL;
	return s;
}

int main(void)
{
	school_t* osu=create_school("osu");
	school_t* michigan=create_school("michigan");

	/* YOUR CODE HERE */
	/* make osu friends with michigan */
	/* leave michigian friends with no one */

	/* examine this code.  what is happening here? */
	printf("osu is friends with %s\n",
			osu->friend!=NULL?osu->friend->name:"no one");
	printf("michigan is friends with %s\n",
			michigan->friend!=NULL?michigan->friend->name:"no one");

	free(osu);
	free(michigan);

	return 0;
}
