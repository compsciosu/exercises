#include <stdio.h>
#include <stdlib.h>

#define ELEMENTS 10000000

int main(void)
{
	int i;

	/* create an array of 10,000,000 integers */
	int a[ELEMENTS];

	/* initialize the array to 0, 1, 2, 3... */
	for (i=0; i<ELEMENTS; i++) {
		a[i]=i;
	}

	/* (1) */
	/* compile and run this program.  what happens?  why? */

	/* (2) */
	/* fix this program by creating 'a' on the heap instead */
	/* use the malloc function to allocate space on the heap */

	printf("a[%d] is %d, a[%d] is %d\n", 0, a[0], ELEMENTS-1, a[ELEMENTS-1]);

	return 0;
}
