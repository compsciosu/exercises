#include <stdio.h>

/* YOUR CODE HERE */
/* Fix the bug by using the const keyword */

int sec_per_hour=3600;

int main(void)
{
	int sec;

	sec=100;
	printf("hours: %f\n", (float)sec/sec_per_hour);

	/* add a second */
	sec++;
	printf("hours: %f\n", (float)sec/sec_per_hour);
	
	/* add an hour */
	sec=sec+sec_per_hour;
	printf("hours: %f\n", (float)sec/sec_per_hour);
	
	/* add two hours */
	sec=sec+2*sec_per_hour;
	printf("hours: %f\n", (float)sec/sec_per_hour);
	
	/* add three hours and two seconds */
	sec=sec+3*sec_per_hour+2;
	printf("hours: %f\n", (float)sec/sec_per_hour);

	/* add four hours and one second */
	sec=sec+3*sec_per_hour++;
	printf("hours: %f\n", (float)sec/sec_per_hour);
	
	/* add an hour and 6 seconds */
	sec=sec+sec_per_hour+6;
	printf("hours: %f\n", (float)sec/sec_per_hour);
	
	/* add a second */
	sec++;
	printf("hours: %f\n", (float)sec/sec_per_hour);
	
	return 0;
}
