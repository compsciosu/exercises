typedef struct stair_t {
	int number;
	struct stair_t* next;
	struct stair_t* prev;
} stair_t;
