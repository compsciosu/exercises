#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include "stairs.h"

/*
 * after a long night of studying/"studying", you don't seem to have the energy
 * to make it up the stairs to your dorm.  thinking that a realistic staircase
 * simulation program might help, you quickly sit down and code up this small
 * program to help plan out your journey home.  fill in the missing code to
 * build a linked list of stairs, so that you can navigate your way back.
 */

stair_t* create_stair(int n)
{
	stair_t* stair=malloc(sizeof(stair_t));
	assert(stair);

	stair->number=n;
	stair->next=NULL;
	stair->prev=NULL;

	return stair;
}

void destroy_stair(stair_t* s)
{
	free(s);
}

stair_t* create_staircase(int count)
{
	int i;
	stair_t* first_stair=create_stair(1);
	stair_t* previous_stair=first_stair;
	stair_t* next_stair;

	assert(count>0);

	for (i=1; i<=count; i++) {
		/* YOUR CODE HERE */

		/* (1) Create a new stair, assign it to next_stair */

		/* (2) Link the previous_stair and next_stair */
		/* (Set previous_stair->next and next_stair->last) */

		/* (3) Save next_stair as the previous_stair */


	}

	/* return the first stair, instead of NULL */
	return NULL;
}

int is_head(stair_t* stair)
{
	/* YOUR CODE HERE */
	/* return 1 if the stair is the head of the linked list, 0 otherwise */
}

int is_tail(stair_t* stair)
{
	/* YOUR CODE HERE */
	/* return 1 if the stair is the tail of the linked list, 0 otherwise */
}

void traverse(stair_t* stair)
{
	while (!is_tail(stair)) {
		if (rand()%2) {
			if (stair->next!=NULL) {
				stair=stair->next;
				printf("... ah, made it to stair %d\n", stair->number);
			}
		}
		else {
			if (stair->prev!=NULL) {
				stair=stair->prev;
				printf("*hic* darnit, fell back to stair %d\n", stair->number);
			}
		}
	}
	printf("woohoo!\n");
}

void destroy_staircase(stair_t* stair)
{
	stair_t* next;
	
	while (stair!=NULL) {
		next=stair->next;
		free(stair);
		stair=next;
	}
}

int main(void)
{
	stair_t* first_stair=create_staircase(10);

	traverse(first_stair);

	destroy_stair(first_stair);

	return 0;
}
