#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define MAX_LIBRARY_NAME 128

typedef struct library_t {
	char name[MAX_LIBRARY_NAME];
	struct library_t* next;
} library_t;

library_t* create_library(char* name)
{
	library_t* l=malloc(sizeof(library_t));
	assert(l);
	strcpy(l->name, name);
	l->next=NULL;
	return l;
}

void print_list(library_t* head)
{
	library_t* node=head;
	while (node!=NULL) {
		printf("%s\n", node->name);
		node=node->next;
	}
}

int main(void)
{
	library_t *thomson, *sel, *moritz;

	/* YOUR CODE HERE */
	
	/* (1) Create three libraries using the create_library function.
	 * (Hint: three OSU libraries are Thomson, SEL, and Moritz) */

	/* (2) Use the ->next field to link the first library to the second, and the
	 * second to the third. */

	/* (3) Use the print_libraries function to print the list of libraries.
	 * Pass in the first (head) library to print the entire list */

	/* (4) Free the memory used by each library by calling free() on the library
	 * pointer */

	return 0;
}
