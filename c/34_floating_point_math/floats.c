#include <stdio.h>

/* You wrote an awesome app to help track and manage your limited free time! */
/* Unfortunately, it doesn't work, and you're stuck in an endless party cycle. */

int main(void)
{
	double free_time=.3;

	free_time-=.1;
	free_time-=.1;
	free_time-=.1;

	/* YOUR CODE HERE */
	/* Compile and run this program.  What happens?  Why? */
	/* It seems you've mismanaged your free time.  Use an approximate equality
	 * check to fix the bug. */

	if (free_time==0) {
		printf("cry.\n");
	}
	else {
		printf("party!!\n");
	}

	return 0;
}
