#include "dog.h"
#include "big_dog.h"
#include "small_dog.h"

int main(void)
{
	dog_t regular_dog, big_dog, small_dog;

	/* Use function pointers to make the regular dog bark, the big dog BARK, and
	 * the small dog yip */

	/* YOUR CODE HERE */


	/* Now that all the dogs are properly created, we don't have to worry about
	 * what specific type they are - we can just call their "bark" method.  This
	 * is the essence of object oriented programming! */

	regular_dog.bark();
	big_dog.bark();
	small_dog.bark();

	return 0;
}
