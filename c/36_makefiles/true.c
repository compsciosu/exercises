#include <stdio.h>
#include "c.h"
#include "makes.h"
#include "my.h"
#include "life.h"
#include "complete.h"

int main(void)
{
	c();
	makes();
	my();
	life();
	complete();
	return 0;
}
