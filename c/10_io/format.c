#include <stdio.h>

int main(void)
{
	/* modify the format string so that the following prints: */
	/* "hello 3.14 a\n" */
	/* (do not modify any argument except the format string) */
	printf("format string", "hello world!", 3.14159, 97);
	return 0;
}
