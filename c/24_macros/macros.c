#include <stdio.h>

/* YOUR CODE HERE */
/* make a macro called AREA to compute the area of a circle, given a radius */
/* (formula: area = 3.14159 * radius * radius) */
/* (use #define to create a new macro) */

int main(void)
{
	float radius_1 = 1.5f;
	int radius_2 = 4;
	long double radius_3 = 9.19285124123;

	/* macros are commonly used as a simple "function" that can take arguments
	 * of many types.  here, we use AREA() as if it were a function accepting
	 * either ints, floats, or long doubles.  the same functionality with
	 * functions instead of macros would require creating three different
	 * functions - one for each type of argument. */
	printf("area 1: %f\n", AREA(radius_1));
	printf("area 2: %f\n", AREA(radius_2));
	printf("area 3: %Le\n", AREA(radius_3));

	return 0;
}
