#include <stdio.h>

int main(int argc, char** argv)
{
	/* YOUR CODE HERE */
	/* Capitalize and print the first string passed to the program on the command line */
	/* 
	 * For example, if the program is run as:
	 *   ./toupper hello
	 * you should print out
	 *   HELLO
	 */
	/* Use argv[i] to access the ith command line argument, as a string */
	/* argv[0] is always the name of the program */
	
	return 0;
}
