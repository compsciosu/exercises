#include <stdio.h>

void meow(void) {
	printf("meow!\n");
}

void hiss(void) {
	printf("hiss!\n");
}

void yack(void) {
	printf("yack!\n");
}

void do_cat(void (*sound)(void)) {
	sound(); sound(); sound();
}

int main(void)
{
	void (*sound)(void);

	/* YOUR CODE HERE */
	/* make the sound variable point to one of the three cat sound functions  */
	/* then pass sound to the do_cat function to simulate the cat */


	return 0;
}
