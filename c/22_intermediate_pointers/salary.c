#include <stdio.h>

/* increase the supplied salary by 10000 */
void give_raise(int* salary)
{
	/* access the memory pointed to by salary, and increase it by 10000 */
	*salary=*salary+10000;

	/* YOUR CODE HERE */
	/* Using pointers, can you find a way to give poor chris a raise too? */

}

int main(void)
{
	/* create salaries for all employees */
	/* the static keyword is used to let variables preserve their values after a
	 * function exits. */
	static int alice_salary=100000;
	static int bob_salary=90000;
	static int chris_salary=1;

	/* print current salaries */
	printf("\n");
	printf("alice's salary: %d\n", alice_salary);
	printf("bob's salary:   %d\n", bob_salary);
	printf("chris's salary: %d\n", chris_salary);

	/* give yearly raises */
	printf("\n");
	printf("applying yearly raises\n");

	give_raise(&alice_salary);
	give_raise(&bob_salary);
	/* none for chris >:| */
	/* give_raise(&chris_salary); */

	/* print new salaries */
	printf("\n");
	printf("alice's salary: %d\n", alice_salary);
	printf("bob's salary:   %d\n", bob_salary);
	printf("chris's salary: %d\n", chris_salary);

	printf("\n");

	return 0;
}
