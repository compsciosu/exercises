#include <stdio.h>

/* This program is riddled with errors, but none show up when you compile with
 *
 * gcc example.c -o example
 *
 * Find the errors by using the -Wall flag, and fix the program to produce the
 * correct results 
 */

int sum(int x, int y)
{
	int z = x + y;
}

int main(void)
{
	printf("the sum of 1 and 2 is: %d\n", sum(1, 2));
}
