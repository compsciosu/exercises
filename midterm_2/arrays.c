/* COMPILE WITH gcc arrays.c -m32 -masm=intel */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

char array[100];

int main(void)
{
	int i;

	/* initialize array to { 0, 1, 2, 3, ..., 98, 99 } */
	__asm__ __volatile__ ("\
		/* YOUR CODE HERE */  \n\
		                      \n\
		"
		);

	for (i=0; i<100; i++) {
		assert(array[i]==i);
	}

	printf("correct!\n");

	return 0;
}
