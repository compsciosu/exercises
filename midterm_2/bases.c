#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

unsigned int crc32(unsigned char *message);

int main(void)
{
	/* YOUR CODE HERE */
	char a[]="10110"; /* 29 from base 10 to base 2 */
	char b[]="10110"; /* 29 from base 16 to base 2 */
	char c[]="43";    /* 10011 from base 2 to base 10 */
	char d[]="fa09";  /* 57 from base 8 to base 16 */
	char e[]="61";    /* 2102 from base 3 to base 7 */
	char f[]="2c";    /* -21 from base 10 to 1 byte base 16 */

	assert(crc32(a)==0xb9c54081);
	assert(crc32(b)==0x3da181b0);
	assert(crc32(c)==0xd8819d45);
	assert(crc32(d)==0x087882e3);
	assert(crc32(e)==0xff4f5344);
	assert(crc32(f)==0xfaef8d69);

	printf("correct!\n");

	return 0;
}

unsigned int crc32(unsigned char *message) {
	int i, j;
	unsigned int byte, crc, mask;
	i = 0;
	crc = 0xFFFFFFFF;
	while (message[i] != 0) {
		byte = message[i];
		crc = crc ^ byte;
		for (j = 7; j >= 0; j--) {
			mask = -(crc & 1);
			crc = (crc >> 1) ^ (0xEDB88320 & mask);
		}
		i = i + 1;
	}
	return ~crc;
}

