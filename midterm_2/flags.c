/* COMPILE WITH gcc flags.c -masm=intel */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

int main(void)
{
	char sign, overflow, carry, zero;
	char my_sign, my_overflow, my_carry, my_zero;

	/* PART I */

	/* YOUR CODE HERE */

	my_sign=1;
	my_overflow=0;
	my_carry=0;
	my_zero=1;

	/* END YOUR CODE */

	__asm __volatile__ ("\
		                        \n\
		mov eax, 1              \n\
		mov edx, 2              \n\
		add eax, edx            \n\
		                        \n\
		sets %[sign]            \n\
		setc %[carry]           \n\
		seto %[overflow]        \n\
		setz %[zero]            \n\
		"
		:[sign]"+m"(sign),
		 [overflow]"+m"(overflow),
		 [carry]"+m"(carry),
		 [zero]"+m"(zero)
		: /* no input */
		: "eax", "edx"
		);

	assert(sign==my_sign);
	assert(overflow==my_overflow);
	assert(carry==my_carry);
	assert(zero==my_zero);

	/* PART II */

	/* YOUR CODE HERE */

	my_sign=1;
	my_overflow=0;
	my_carry=0;
	my_zero=1;

	/* END YOUR CODE */

	__asm __volatile__ ("\
		                        \n\
		mov ax, 0xfeed          \n\
		mov dx, 0xface          \n\
		add ax, dx              \n\
		                        \n\
		sets %[sign]            \n\
		setc %[carry]           \n\
		seto %[overflow]        \n\
		setz %[zero]            \n\
		"
		:[sign]"+m"(sign),
		 [overflow]"+m"(overflow),
		 [carry]"+m"(carry),
		 [zero]"+m"(zero)
		: /* no input */
		: "eax", "edx"
		);

	assert(sign==my_sign);
	assert(overflow==my_overflow);
	assert(carry==my_carry);
	assert(zero==my_zero);

	/* PART III */

	/* YOUR CODE HERE */

	my_sign=1;
	my_overflow=0;
	my_carry=0;
	my_zero=1;

	/* END YOUR CODE */

	__asm __volatile__ ("\
		                        \n\
		mov eax, 0              \n\
		mov edx, 1              \n\
		sub eax, edx            \n\
		                        \n\
		sets %[sign]            \n\
		setc %[carry]           \n\
		seto %[overflow]        \n\
		setz %[zero]            \n\
		"
		:[sign]"+m"(sign),
		 [overflow]"+m"(overflow),
		 [carry]"+m"(carry),
		 [zero]"+m"(zero)
		: /* no input */
		: "eax", "edx"
		);

	assert(sign==my_sign);
	assert(overflow==my_overflow);
	assert(carry==my_carry);
	assert(zero==my_zero);

	/* PART IV */

	/* YOUR CODE HERE */

	my_sign=1;
	my_overflow=0;
	my_carry=0;
	my_zero=1;

	/* END YOUR CODE */

	__asm __volatile__ ("\
		                        \n\
		mov eax, 1              \n\
		mov edx, 1              \n\
		sub eax, edx            \n\
		                        \n\
		sets %[sign]            \n\
		setc %[carry]           \n\
		seto %[overflow]        \n\
		setz %[zero]            \n\
		"
		:[sign]"+m"(sign),
		 [overflow]"+m"(overflow),
		 [carry]"+m"(carry),
		 [zero]"+m"(zero)
		: /* no input */
		: "eax", "edx"
		);

	assert(sign==my_sign);
	assert(overflow==my_overflow);
	assert(carry==my_carry);
	assert(zero==my_zero);

	/* PART V */

	/* YOUR CODE HERE */

	uint32_t eax=0;
	uint32_t edx=0;

	/* END YOUR CODE */

	__asm __volatile__ ("\
		                        \n\
		mov eax, %[eax]         \n\
		mov edx, %[edx]         \n\
		add eax, edx            \n\
		                        \n\
		sets %[sign]            \n\
		setc %[carry]           \n\
		seto %[overflow]        \n\
		setz %[zero]            \n\
		"
		:[sign]"+m"(sign),
		 [overflow]"+m"(overflow),
		 [carry]"+m"(carry),
		 [zero]"+m"(zero)
		:[eax]"m"(eax),
		 [edx]"m"(edx)
		: "eax", "edx"
		);

	assert(sign==1);
	assert(overflow==1);
	assert(carry==1);
	assert(zero==0);

	printf("correct!\n");

	return 0;
}
