/* COMPILE WITH gcc cdecl_1.c -m32 -masm=intel */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

int get_greater(int x, int y);

/* implement the get_greater function in assembly */
/* note: you do not need to know inline constraints for this */
__asm__ ("\
	/* YOUR CODE HERE */    \n\
	get_greater:            \n\
	"
	);

int main(void)
{
	assert(get_greater(1, 2)==2);
	assert(get_greater(3, 2)==3);

	printf("correct!\n");

	return 0;
}
