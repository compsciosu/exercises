/* COMPILE WITH gcc cdecl_1.c -m32 -masm=intel */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/mman.h>

uint32_t original_esp;
uint32_t x_esp, x_ebp, x_eax;

int main(void)
{
	uint32_t my_esp, my_ebp, my_eax;

	/* setup space for a new stack at a known address */
	void* stack_buffer=mmap(
			(void*)0x10000,
			4096,
			PROT_READ|PROT_WRITE,
			MAP_SHARED|MAP_ANONYMOUS,
			-1,
			0
			);
	void* stack=stack_buffer+4096;

	/* YOUR CODE HERE */
	my_eax=0;
	my_esp=0;
	my_ebp=0;
	/* END YOUR CODE */

	__asm__ __volatile__ ("\
			mov %[original_esp], esp   \n\
			mov esp, %[stack]          \n\
			                           \n\
			/* starting values:        \n\
			   esp: 0x11000            \n\
			   ebp: unknown            \n\
			   eax: unknown */         \n\
			                           \n\
			push ebp                   \n\
			mov  ebp, esp              \n\
			sub  esp, 4                \n\
			mov  dword ptr [ebp-4], 3  \n\
			mov  eax, [ebp-4]          \n\
			mov  edx, 2                \n\
			mul  edx                   \n\
			mov  [ebp-4], eax          \n\
			mov  eax, [ebp-4]          \n\
			                           \n\
			/* solve for eax here */   \n\
			/* solve for ebp here */   \n\
			/* solve for esp here */   \n\
			mov %[x_ebp], ebp          \n\
			mov %[x_esp], esp          \n\
			mov %[x_eax], eax          \n\
			                           \n\
			mov  esp, ebp              \n\
			pop  ebp                   \n\
			                           \n\
			mov  esp, %[original_esp]  \n\
			"
			: [original_esp]"=m"(original_esp),
			  [x_eax]"=m"(x_eax),
			  [x_ebp]"=m"(x_ebp),
			  [x_esp]"=m"(x_esp)
			: [stack]"m"(stack)
			:
			);

	printf("%08x\n", x_esp);
	printf("%08x\n", x_eax);
	printf("%08x\n", x_ebp);

	assert(x_eax==my_eax);
	assert(x_ebp==my_ebp);
	assert(x_esp==my_esp);

	printf("correct!\n");

	munmap(stack_buffer, 4096);

	return 0;
}
