/*
 * Compile the program with:
 *   gcc -m32 address.c -o address
 *
 * Load the program in gdb:
 *   gdb ./address
 *
 * Change the disassembly flavor to Intel syntax:
 *   set disassembly-flavor intel
 *
 * Start the program in the debugger:
 *   start
 *
 * Examine the program:
 *   disas
 *
 * Determine the following:
 *   - Where is sum being calculated?
 *   - How is the variable 'a' being accessed?  Why?
 *   - How is the variable 'b' being accessed?  Why?
 *   - How is the variable 'c' being accessed?  Why?
 *
 * Set a breakpoint on the code after the sum is calculated (use the "b"
 * command, type "help b" for more information).
 *
 * Run the program to the breakpoint.  What is the sum of the three numbers?
 */

#include <stdio.h>

int a=01337;

int main(void)
{
	int b=0xc0ffee;
	int* c=&b;
	
	int sum=a+b+*c;

	return 0;
}
