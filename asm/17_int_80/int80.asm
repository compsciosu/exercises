; With your knowledge of int 0x80, use google to determine which system calls are
; being made by the following program.  What is this program doing?
; (Build with make)
global _start
 
section .text
_start:

loop:
	mov  eax, 3
	mov  ebx, 0
	lea  ecx, [buffer]
	mov  edx, 1
	int  0x80

    mov eax, 4
    mov ebx, 1
    lea ecx, [buffer]
    mov edx, 1
    int 0x80

	mov eax, 0
	mov al, [buffer]
	cmp eax, 10
	jne loop
 
    mov eax, 1
    mov ebx, 0
    int 0x80
 
section .data
buffer: times 16 db 0
