/*
 * 1) Compile the program with: gcc -m32 answer.c -o answer
 * 2) Debug the program with gdb ./answer
 * 3) Set a breakpoint on the printf: b printf
 * 4) Run the program: run
 * 5) Examine the registers: info reg
 * 6) Which register did gcc pick to store the "answer" variable?
 * 7) Can you use gdb to make the program say the answer is 43?
 */

#include <stdio.h>

int main(void)
{
	int answer=42;

	printf("the answer is: %d\n", answer);

	return 0;
}
