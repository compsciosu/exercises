/* Compile the program with gcc -m32 overflow.c -o overflow */
/* Run with ./overflow */
/* How many iterations deep does f get before it crashes? */
/* Which direction does the stack grow?  How can you tell? */
/* What causes the program to crash? */

#include <stdio.h>

void f(int i)
{
	printf("iteration %d, my stack is around %08x\n", i, (unsigned int)&i);
	f(i+1);
}

int main(void)
{
	f(1);
	return 0;
}
