; build the program with
;   nasm -felf arrays.asm
;   ld -melf_i386 arrays.o -o arrays

; debug the program with:
;   gdb ./arrays
;   b _start
;   run
; 
; use x/10i $eip to examine the upcoming instructions
; use info reg to examine registers
; use si to step forward
; use x/10x $esp to examine the stack
; repeat to examine each line
; use run to start over

global _start
 
section .text

; begin our program
_start:
	mov esp, stack

	; YOUR CODE HERE
	; Write assembly to initialize the x array to {1, 2, 3, ... 128}
	; Use gdb to check your result.

	; exit
	mov eax, 1
	mov ebx, 0
	int 0x80
 
section .data

; x is an array of 128 bytes, initialized to 0
x: times 128 db 0

times 128 db 0
stack equ $-4
