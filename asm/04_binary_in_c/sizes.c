#include <stdio.h>

int main(void)
{
	unsigned int x=708296926;

	/* YOUR CODE HERE */

	/* 1) Use printf and the %d and %08x placeholder tokens to view the
	 * decimal and hexadecimal representation of x */

	/* 2) An unsigned integer is 4 bytes.  Cast x to an unsigned short (2
	 * bytes), and print the decimal and hexadecimal result */

	/* 3) Cast x to an unsigned char (1 byte) and print the decimal and
	 * hexadecimal result. */

	/* 4) Compile and run steps 1-3.  What do you notice about the relationship
	 * between the hexadecimal representation and bytes? */

	/* 5) Use pointers to set the high byte of x to 0x13.  Print out the new
	 * decimal and hexadecimal form of x. */


	return 0;
}
