#include <stdio.h>

int main(void)
{
	unsigned int x=1;
	signed int y=-1;

	/* YOUR CODE HERE */
	/* First, compile and run the program.  What happens? */
	/* Next, use the printf trick from the slides to examine the raw bits of x
	 * and y.  Why are they stored the way they are? */
	/* Finally examine the comparison.  Recall that in order to operate on two
	 * different types, the computer must first convert (cast) one type to the
	 * other.  What is happening with the implicit conversion that causes the
	 * condition to pass? */

	if (y > x) // -1 > 1
	{
		printf("yep, %d is greater than %d\n", y, x);
		printf("let's go home. computers don't work.\n");
	}

	return 0;
}
