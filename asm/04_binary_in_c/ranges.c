#include <stdio.h>

int main(void)
{
	int i;
	unsigned char x=0;

	/* YOUR CODE HERE */

	/* 1) Use printf and the %d and %02x
	 * placeholder tokens to view the decimal and
	 * hexadecimal representation of x */
	printf("%d %02x\n", x, x);

	/* 2) In a for loop, increment x by 1.  Repeat
	 * this 300 times, printing out the new
	 * decimal and hexadecimal result each time.
	 * What happens when x reaches 256? */
	for (i=0; i<300; i++) {
		x++;
		printf("%d %02x\n", x, x);
	}

	return 0;
}
