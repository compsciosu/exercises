#include <stdio.h>
#include <assert.h>

int get_sign_bit(signed int x)
{
	int sign_bit;

	/* YOUR CODE HERE */

	/* Use bit shifting (<< and >>) and binary AND (&) (referred to as masking)
	 * to extract the sign bit from x */
	sign_bit=(x>>31)&1;


	return sign_bit;
}

int main(void)
{
	signed int x[]={0xf091ab4d, 0x0192bfff, 0x91ddee10, 0x22222222};

	assert(get_sign_bit(x[0])==1);
	assert(get_sign_bit(x[1])==0);
	assert(get_sign_bit(x[2])==1);
	assert(get_sign_bit(x[3])==0);

	printf("correct!\n");

	return 0;
}
