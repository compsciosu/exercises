#include <stdio.h>
#include <assert.h>

signed int get_twos_complement(signed int x)
{
	signed int twos;

	/* YOUR CODE HERE */

	/* Use bit inversion (~) and addition to compute the two's complement of x */

	return twos;
}

int main(void)
{
	signed int x[]={4, -9, 100, -9301};

	printf("two(%08x) is %08x\n", x[0], get_twos_complement(x[0]));
	printf("two(%08x) is %08x\n", x[1], get_twos_complement(x[1]));
	printf("two(%08x) is %08x\n", x[2], get_twos_complement(x[2]));
	printf("two(%08x) is %08x\n", x[3], get_twos_complement(x[3]));

	assert(get_twos_complement(x[0])==-x[0]);
	assert(get_twos_complement(x[1])==-x[1]);
	assert(get_twos_complement(x[2])==-x[2]);
	assert(get_twos_complement(x[3])==-x[3]);

	printf("correct!\n");

	return 0;
}
