#include <stdio.h>

/* Compile this program:
 *
 * gcc hello.c -o hello
 *
 * Then open the executable in vim:
 *
 * vim hello
 *
 * What are we seeing?  Why?
 */

int main(void)
{
	printf("hello, world!\n");
	return 0;
}
