; examine the assembly code.

; build with: 
;   nasm building.asm -felf -o building.o && ld -melf_i386 building.o -o building

; run with:
;   ./building

; examine and understand the code below. 
; - how does looping work in assembly?
; - how do global variables work?
; - what is happening when we say "mov eax, [x]"?

; use gdb to debug the program and determin the final value of y.

; hint: research and use these commands in gdb:
; - set disassembly-flavor intel (change to intel syntax)
; - b (set breakpoint)
; - run (begin execution)
; - c (continue after breakpoint)
; - info reg (show registers)
; - display (show data at label)

global _start
 
section .text

; begin our program
_start:

	; add x to y 0x1337 times
loop:
	mov eax, [x]
	mov edx, [y]
	add edx, eax
	mov [y], edx
	mov eax, [counter]
	sub eax, 1
	mov [counter], eax
	cmp eax, 0
	jne loop

	; exit
	mov eax, 1
	mov ebx, 0
	int 0x80
 
section .data
counter: dd 0x1337
x: dd 0x1337
y: dd 0
