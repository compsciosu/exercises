; build the program with
;   nasm -felf instructions.asm
;   ld -melf_i386 instructions.o -o instructions

; debug the program with:
;   gdb ./instructions
;   b _start
;   run
; 
; use disas to examine the upcoming instructions
; use info reg to examine registers
; use si to step forward
; repeat to examine each line
; use run to start over

global _start
 
section .text

; begin our program
_start:
	mov eax, [grade]
	cmp eax, 'C'

	jle wake_up

celebrate:
	mov edx, [food]
	jmp done

wake_up:
	mov edx, [coffee]
	jmp done

done:
	; * what value is in edx here?
	; * use gdb to step through the program and verify your answer

	; exit
	mov eax, 1
	mov ebx, 0
	int 0x80
 
section .data
grade: dd 'B'
coffee: dd 0xc0ffee
food: dd 0xf00d
