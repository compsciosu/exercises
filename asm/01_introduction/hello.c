#include <stdio.h>

/*
 * We're now moving deeper into the computer; our goal is to understand how high
 * level languages like Java and C become the ones and zeros that control our
 * systems.  Assembly forms the bridge between the languages we write and the
 * bits the computer executes.
 *
 * Examine the intermediate assembly language generated from a simple hello
 * world program.  On the command line, run:
 *
 * gcc -S hello.c && cat hello.s
 */

int main(void)
{
	printf("Hello, world!\n");
	return 0;
}
