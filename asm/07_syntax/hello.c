#include <stdio.h>

/* Compile this program:
 *
 *   gcc hello.c -o hello
 *
 * View the AT&T syntax of the program's assembly:
 *
 *   objdump -d hello
 *
 * View the Intel syntax of the program's assembly:
 *
 *   objudmp -d -Mintel hello
 *
 * What are the differences?
 */

int main(void)
{
	printf("hello, world!\n");
	return 0;
}
