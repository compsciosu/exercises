; build the program with
;   nasm -felf stack.asm
;   ld -melf_i386 stack.o -o stack

; debug the program with:
;   gdb ./stack
;   b _start
;   run
; 
; use x/10i $eip to examine the upcoming instructions
; use info reg to examine registers
; use si to step forward
; use x/10x $esp to examine the stack
; repeat to examine each line
; use run to start over

global _start
 
section .text

; begin our program
_start:
	mov esp, stack
	push 0xc0ffee
	push 0xf00fb00f
	push 0x13371337

	; * your code here
	; use pop instructions to set
	; eax to 0xc0ffee
	; ebx to 0xf00fb00f
	; ecx to 0x13371337

	; exit
	mov eax, 1
	mov ebx, 0
	int 0x80
 
; how is the stack being created?
; what does the below code do?
section .data
times 128 db 0
stack equ $-4
