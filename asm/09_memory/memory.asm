; build the program with
;   nasm -felf memory.asm
;   ld -melf_i386 memory.o -o memory

; debug the program with:
;   gdb ./memory
;   b _start
;   run
; 
; use disas to examine the upcoming instructions
; use info reg to examine registers
; use si to step forward
; repeat to examine each line, and which values are being loaded
; use run to start over

global _start
 
section .text

; begin our program
_start:
	; * what is the difference between these two lines?
	mov eax, value
	mov ebx, [value]

	; * what does this line do?
	mov ecx, [eax]

	; * YOUR CODE HERE
	; use the "value" label to load 0x1337c0de into eax

	; exit
	mov eax, 1
	mov ebx, 0
	int 0x80
 
section .data
value: dd 0xc0ffee, 0x1337c0de
