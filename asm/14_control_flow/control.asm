; build with: 
;   nasm control.asm -felf -o control.o && ld -melf_i386 control.o -o control

; Examine the control flow of the following program.
; What must x's starting value be to print out "correct!"?
; Modify x so that the program prints correct.

global _start
 
section .text

_start:

loop:
	mov eax, [x]
	mov ecx, 2
	mul ecx

	cmp eax, 2673
	jle print_wrong
	cmp eax, 2675
	jge print_wrong

print_correct:
	; print "correct!"
	mov eax, 4 ; write
	mov ebx, 1 ; stdout
	mov ecx, right
	mov edx, right.len
	int 0x80
	jmp done

print_wrong:
	; print "nope!"
	mov eax, 4 ; write
	mov ebx, 1 ; stdout
	mov ecx, wrong
	mov edx, wrong.len
	int 0x80
	jmp done

done:
	; exit
	mov eax, 1
	mov ebx, 0
	int 0x80
 
section .data
x: dd 0
right:  db  "Correct!", 10
.len:   equ $ - right
wrong:  db  "Nope!", 10
.len:   equ $ - wrong

