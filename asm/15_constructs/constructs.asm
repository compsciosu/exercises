; build with: 
;   make

; The following C code counts the number of unique factors of a number n:

; int i, f=0;
; for (i=1; i<=n; i++) {
;   if (n%i==0) {
;     f++;
;   }
; }

; Use your knowledge of control flow constructs in assembly to write x86 code
; that counts the number of factors of the number in eax, and stores the result
; in ecx

global _start
 
section .text

_start:

	mov eax, 3628800

	; YOUR CODE HERE

	not ecx
	cmp ecx, 0xfffffef1
	je print_correct
	jmp print_wrong

print_correct:
	; print "correct!"
	mov eax, 4 ; write
	mov ebx, 1 ; stdout
	mov ecx, right
	mov edx, right.len
	int 0x80
	jmp done

print_wrong:
	; print "nope!"
	mov eax, 4 ; write
	mov ebx, 1 ; stdout
	mov ecx, wrong
	mov edx, wrong.len
	int 0x80
	jmp done

done:
	; exit
	mov eax, 1
	mov ebx, 0
	int 0x80
 
section .data
right:  db  "Correct!", 10
.len:   equ $ - right
wrong:  db  "Nope!", 10
.len:   equ $ - wrong

