#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int binary_to_decimal(char* binary)
{
	int result=0;
	int len=strlen(binary);
	int counter=0;

	/* iterate over each digit of the binary number */
	while (counter<len) {
		/* starting at the right, grab a character ('1' or '0') representing the
		 * digit */
		int digit=binary[len-counter-1];
		
		/* convert the character from a character ('1' or '0') to a number (1 or
		 * 0) */
		digit=digit-'0';

		/* digit is now 0 or 1 */

		/* YOUR CODE HERE */
		/* using the stream of digits, convert the binary number to a decimal
		 * number */
		/* HINT: the pow() function from math.h might help. */



		counter++;
	}

	return result;
}

int main(void)
{
	char binary[128];

	printf("enter a binary number:\n");
	scanf("%s", binary);
	printf("\n");

	printf("in decimal, that's:\n");
	printf("%d\n", binary_to_decimal(binary));
	printf("\n");

	return 0;
}
