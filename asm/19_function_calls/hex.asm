; build the program with
;   nasm -felf hex.asm
;   ld -melf_i386 hex.o -o hex

; debug the program with:
;   gdb ./hex
;   b _start
;   run
; 
; gdb reference:
;   use x/10i $eip to examine the upcoming instructions
;   use info reg to examine registers
;   use si to step forward
;   use b *address to set breakpoints
;   use c to continue execution
;   use run to start over

global _start
 
section .text

; begin our program
_start:
	; set up the stack
	mov esp, stack
	mov ebp, stack

	; YOUR CODE HERE

	; overview:
	; read a number in from the user (assume 0-15) using read_integer.
	; convert it to its hexadecimal equivalent with convert_hex_digit.
	; print the digit to stdout using print_digit.

	; 1) create two local variables, one to store the user input, and one to
	;    store the converted digit
	; 2) call read_integer, and store the result in one of the local variables.
	; 3) pass the integer to convert_hex_digit, and store the result in the other
	;    local variable.
	; 4) pass the result to print_digit.

	; exit
	mov eax, 1
	mov ebx, 0
	int 0x80

; int convert_hex_digit(int);
; convert the parameter to a hex character equivalent
; the result is returned in eax
convert_hex_digit:
	push ebp         ; save the previous stack frame
	mov ebp, esp     ; load a new stack frame

	mov eax, [ebp+8] ; load first parameter

	cmp eax, 0       ; check against lower bounds
	jl  invalid      ; invalid if less than 0
	cmp eax, 15      ; check against upper bounds
	jg  invalid      ; invalid if greater than 0

	cmp eax, 9       ; is parameter between 0 and 9?
	jle digit09      ; convert to '0'-'9'

digitaf:             ; convert to ascii digit 'a' - 'f'
	sub eax, 10      
	add eax, 'a'     
	jmp done

digit09:             ; convert to ascii digit '0' - '9'
	add eax, '0'
	jmp done

invalid:
	mov eax, -1      ; load invalid result
	jmp done         ; finished

done:
	mov esp, ebp     ; tear down the current stack frame
	pop ebp          ; restore the previous stack frame
	ret              ; return to the caller


; int read_integer(void);
; read a 32 bit unsigned number from stdin
; the result is returned in eax
read_integer:
                             ; set up stack frame
    push ebp                 ; save the current stack frame
    mov  ebp, esp            ; set up a new stack frame

                             ; set up local variables
    sub  esp, 8              ; allocate space for two local ints
    mov  dword [ebp-4], '0'  ; digit: initialize to '0' 
    mov  dword [ebp-8], 0    ; value: initialize to 0

                             ; save modified registers
    push ebx                 ; save ebx

.read_loop:
                             ; update number calculation
    mov  eax, 10             ; load multiplier
    mul  dword [ebp-8]       ; multiply current value by 10, store in eax
    add  eax, [ebp-4]        ; add new digit to current value
    sub  eax, '0'            ; convert digit character to numerical equivalent
    mov  [ebp-8], eax        ; save new value

                             ; read in digit from user
    mov  eax, 3              ; syscall 3 (read)
    mov  ebx, 0              ; file descriptor (stdin)
    lea  ecx, [ebp-4]        ; pointer to data to save to
    mov  edx, 1              ; byte count
    int  0x80                ; issue system call

                             ; loop until enter is pressed
    cmp  dword [ebp-4], 10   ; check if end of line reached
    jne  .read_loop          ; if not, continue reading digits

                             ; cleanup
    mov  eax, [ebp-8]        ; save final value in eax
    pop  ebx                 ; restore ebx
    mov  esp, ebp            ; free local variables
    pop  ebp                 ; restore ebp
    ret                      ; return to caller

; void print_digit(int digit)
; print one digit to stdout
; the digit is passed on the stack as the single parameter to the function
print_digit:
                             ; set up stack frame
    push ebp                 ; save the current stack frame
    mov  ebp, esp            ; set up a new stack frame
    push ebx                 ; save ebx

                             ; print character to stdout
    mov  eax, 4              ; syscall 4 (write)
    mov  ebx, 1              ; file descriptor (stdout)
    lea  ecx, [ebp+8]        ; pointer to data to write
    mov  edx, 1              ; byte count
    int  0x80                ; issue system call

                             ; cleanup
    pop  ebx                 ; restore ebx
    mov  esp, ebp            ; free local variables
    pop  ebp                 ; restore ebp
    ret                      ; return to caller
 
section .data
times 128 db 0
stack equ $-4
