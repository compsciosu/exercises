; build the program with
;   nasm -felf functions.asm
;   ld -melf_i386 functions.o -o functions

; debug the program with:
;   gdb ./functions
;   b _start
;   run
; 
; use x/10i $eip to examine the upcoming instructions
; use info reg to examine registers
; use si to step forward
; use b *address to set breakpoints
; use c to continue execution
; use run to start over

global _start
 
section .text

; begin our program
_start:
	; set up the stack
	mov esp, stack

	push 5       ; push the second argument onto the stack
	push 3       ; push the first argument onto the stack
	call add_two ; call add_two
	add esp, 8   ; remove the arguments from the stack

	; set a breakpoint here to check your program

	; exit
	mov eax, 1
	mov ebx, 0
	int 0x80

add_two:
	push ebp      ; save the previous stack frame
	mov ebp, esp  ; load a new stack frame

	; YOUR CODE HERE
	; add the two parameters passed to the function,
	; and return the results in eax

	mov esp, ebp  ; tear down the current stack frame
	pop ebp       ; restore the previous stack frame
	ret           ; return to the caller
 
section .data
times 128 db 0
stack equ $-4
