; build the program with
;   nasm -felf pointers.asm
;   ld -melf_i386 pointers.o -o pointers

; debug the program with:
;   gdb ./pointers
;   b _start
;   run
; 
; use x/10i $eip to examine the upcoming instructions
; use info reg to examine registers
; use si to step forward
; use x/10x $esp to examine the stack
; repeat to examine each line
; use run to start over

global _start
 
section .text

; begin our program
_start:
	mov esp, stack

	; YOUR CODE HERE
	; Use mov instructions and x to set z to 1337
	; Use gdb to check your result.

	; exit
	mov eax, 1
	mov ebx, 0
	int 0x80
 
section .data

x: dd y
y: dd z
z: dd 0

times 128 db 0
stack equ $-4
